<?php
    $file = 'includes/header.php';
    if (file_exists($file) && is_readable($file))
            {   include($file); }
?>
<!--******************************END HEADER***************************************-->
	<nav>
		<a href='index.php' class='button'>Home</a>
		<a href='bio.php' class='button'>Biography</a>
		<a href='discography.php' class='button'>Discography</a>
		<a href='contact.php' class='button selected'>Contact</a>
	</nav>
<!--******************************Form***************************************-->
<?php
	function printform(){
		echo "<div class='wrapper_c'>
	<div class='inner_c'>
	<h1>Contact</h1>
				<form method='post' action='submit_form.php'>
                    <div class='formentry'>
                        <label for='name'>Name&#42;:</label>
                        <input type='text' name='name' id='name' placeholder='Your name' required='required' autofocus size='35' />
                    </div>

                    <div class='formentry'>
                        <label for='email'>Email&#42;:</label>
                        <input type='email' name='email' id='email' placeholder='Your email' required='required' size='35'  />
                    </div>

                    <div class='formentry'>
                    <label for='comments'>Your message:</label>
                    <textarea  placeholder='Enter your message here' name='comments' id='comments' cols='36' rows='5'>
                    </textarea>
                    </div>


                    <div class='clear_fix'></div>
                    <input id='submit' type='submit' value='Submit Form' />
                </form>
    </div>
</div>";
	}

	function spamcheck($field)
				  {
				  //filter_var() sanitizes the e-mail
				  //address using FILTER_SANITIZE_EMAIL
				  $field=filter_var($field, FILTER_SANITIZE_EMAIL);

				  //filter_var() validates the e-mail
				  //address using FILTER_VALIDATE_EMAIL
				  if(filter_var($field, FILTER_VALIDATE_EMAIL))
				    {
				    return TRUE;
				    }
				  else
				    {
				    return FALSE;
				    }
				  }

	if (isset($_POST['email']))
					  {//if "email" is filled out, proceed

					  //check if the email address is invalid
					  $mailcheck = spamcheck($_REQUEST['email']);
						  if ($mailcheck==FALSE)
						    {
						    echo "<span class='submition_results'>Invalid email address!</span>";
						    printform();
						    }
						  elseif($_POST["name"]==" "){
						  	echo "<span class='submition_results'>You need to provide your name!</span>";
						  	printform();
						  }
						   else{//send email
									  	$cc=" ";
										$to="dj_frombg@yahoo.co.uk";
										//$to="julybeat31@gmail.com";
										$subject="Contact form from website!";
										$name=$_POST["name"];
										//$tel=$_POST["tel"];
										$email=$_POST["email"];
										$comments=$_POST["comments"];
										$ourmail="djfrombg@yahoo.co.uk";
										$message="<h2>Dear Yordan,</h2>"."\n ".
			"<h3>This is a new message from the website form</h3>" ."\n ".
			"<p>From: " .$name."</p>\n ".
			"<p>Email: ".$email. "</p>\n".
			"<p>Comments: ".$comments."</p>\n".
			//Sednd a confirmation email to the customer ======================================================================
			$message2="<h2>Dear ".$name.",</h2>" ."\n "."<p>Thank you for contacting us.</p>
			<p>Here is a summary of your request:</p>
			<p>Your name(s): " .$name."</p>" ."\n "."<p>Your contact Email: ".$email. "</p>" ."\n "."<p>Comments: ".$comments;
										// Always set content-type when sending HTML email
										$headers = "MIME-Version: 1.0" . "\r\n";
										$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

										// More headers
										$headers .= 'From: ' .$email. "\r\n";
										$headers .= 'Cc: ' . $cc. "\r\n";
										//send email to us
										mail($to,$subject,$message,$headers);
										//send email back to the customer with his submitted data
										mail($email,'YoVe-Thank You for contacting us' ,$message2,"MIME-Version: 1.0" . "\r\n"."Content-type:text/html;charset=iso-8859-1" . "\r\n"." From: ".$ourmail);
										//print success message on the page
										echo"<section class='submition_results'>
													<h3>Message sent</h3>
													<p>Thank You, <span class='annotation'>".$name."</span> for contacting us!Here is a summary of your request:</p>
														<ul>
															<li>Comments:<span class='annotation'>".$comments."</span></li>
														</ul>


												</section>";
										printform();

										}//end else
					  	}//end if
				else{//if "email" is NOT filled out, print form

				printform();
				}

?>
<script>
	var name=<?php echo $name;?>;
	var email=<?php echo $email;?>;
	var tel=<?php echo $tel;?>;
	if($("#name").val()!="" ||$("#email").val()!=""){
		$("#submit").click(function(){
			$("#name").val(name);
			$("#email").val(email);
			$("#tel").val(tel);
		});
	}
</script>
<script type="text/javascript">   //csroll to top script
			window.addEventListener("load", function () {
			// Set a timeout...
			setTimeout(function () {
			// Hide the address bar!
			window.scrollTo(0, 1);
			}, 0);
			});
		</script>
<!--******************************FOOTER***************************************-->
<?php
    $file2 = 'includes/footer.php';
    if (file_exists($file2) && is_readable($file2))
            {   include($file2); }
?>