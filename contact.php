<?php
    $file = 'includes/header.php';
    if (file_exists($file) && is_readable($file))
            {   include($file); }
?>
<!--******************************END HEADER***************************************-->

	<nav>
		<a href='index.php' class='button'>Home</a>
		<a href='bio.php' class='button'>Biography</a>
		<a href='discography.php' class='button'>Discography</a>
		<a href='#' class='button selected'>Contact</a>
	</nav>
<div class='wrapper_c'>
	<div class='inner_c'>
	<h1>Contact</h1>
				<form method='post' action='submit_form.php'>
                    <div class='formentry'>
                        <label for='name'>Name&#42;:</label>
                        <input type='text' name='name' id='name' placeholder='Your name' required='required' autofocus size='35' />
                    </div>

                    <div class='formentry'>
                        <label for='email'>Email&#42;:</label>
                        <input type='email' name='email' id='email' placeholder='Your email' required='required' size='35'  />
                    </div>

                    <div class='formentry'>
                    <label for='comments'>Your message:</label>
                    <textarea  placeholder='Enter your message here' name='comments' id='comments' cols='36' rows='5'>
                    </textarea>
                    </div>


                    <div class='clear_fix'></div>
                    <input id='submit' type='submit' value='Submit Form' />
                </form>
    </div>
</div>


<!--******************************FOOTER***************************************-->
<?php
    $file2 = 'includes/footer.php';
    if (file_exists($file2) && is_readable($file2))
            {   include($file2); }
?>