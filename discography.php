<?php
    $file = 'includes/header.php';
    if (file_exists($file) && is_readable($file))
            {   include($file); }
?>
<!--******************************END HEADER***************************************-->
	<nav>
		<a href='index.php' class='button'>Home</a>
		<a href='bio.php' class='button'>Biography</a>
		<a href='#' class='button selected'>Discography</a>
		<a href='contact.php' class='button'>Contact</a>
	</nav>
<!-- ====================== Discography  ====================================================== -->
<div class="wrapper_d">
	<!-- left panel of songs -->
	<div class="inner">
		<h1 class="h1d">Discography</h1>
		<div class="soundcloud">
		<iframe width="24.5%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/200343417&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
		
		<iframe width="24.5%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/199684354&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
		
		<iframe width="24.5%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/199669149&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
		
		<iframe width="24.5%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/199665968&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
		
		<iframe width="24.5%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/199605528&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
		
		<iframe width="24.5%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/199516844&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
		
		<iframe width="24.5%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/199515089&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
		
		<iframe width="24.5%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/199512948&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
		</div>
		<!-- <h2 class="h1d">Full song versions available on <a href="https://soundcloud.com/ergo-music" target="_blank">Soundcloud</a></h2>
		<div class="left_d">
			<h2>Early years samples</h2>
			<label for="1first">Cosmic Dust</label>
			<audio name="1first" controls>
			  <source src="audio/Old/Yo_Ve_-_Cosmic_Dust.ogg" type="audio/ogg">
			  <source src="audio/Old/Yo_Ve_-_Cosmic_Dust.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="2first">Deadman Wonderland</label>
			<audio name="2first" controls>
			  <source src="audio/Old/Yo_Ve_-_Deadman_Wonderland.ogg" type="audio/ogg">
			  <source src="audio/Old/Yo_Ve_-_Deadman_Wonderland.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="3first">Distorted Reality</label>
			<audio name="3first" controls>
			  <source src="audio/Old/Yo_Ve_-_Distorted_Reality.ogg" type="audio/ogg">
			  <source src="audio/Old/Yo_Ve_-_Distorted_Reality.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="4first">Eternal Sunshine</label>
			<audio name="4first" controls>
			  <source src="audio/Old/Yo_Ve_-_Eternal_Sunshine.ogg" type="audio/ogg">
			  <source src="audio/Old/Yo_Ve_-_Eternal_Sunshine.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>

		    <label for="5first">Fatal Attraction</label>
			<audio name="5first" controls>
			  <source src="audio/Old/Yo_Ve_-_Fatal_Attraction.ogg" type="audio/ogg">
			  <source src="audio/Old/Yo_Ve_-_Fatal_Attraction.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="6first">Gamma Ray</label>
			<audio name="6first" controls>
			  <source src="audio/Old/Yo_Ve_-_Gamma_Ray.ogg" type="audio/ogg">
			  <source src="audio/Old/Yo_Ve_-_Gamma_Ray.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="7first">Genjutsu</label>
			<audio name="7first" controls>
			  <source src="audio/Old/Yo_Ve_-_Genjutsu.ogg" type="audio/ogg">
			  <source src="audio/Old/Yo_Ve_-_Genjutsu.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="8first">Instrinct</label>
			<audio name="8first" controls>
			  <source src="audio/Old/Yo_Ve_-_Instrinct.ogg" type="audio/ogg">
			  <source src="audio/Old/Yo_Ve_-_Instrinct.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
		</div>

		<div class="left_d2">
			<h2>Latest tracks samples</h2>
			<label for="9first">Brainwash</label>
			<audio name="9first" controls>
			  <source src="audio/New/Yo_Ve_-_Brainwash.ogg" type="audio/ogg">
			  <source src="audio/New/Yo_Ve_-_Brainwash.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="10first">Curiosity</label>
			<audio name="10first" controls>
			  <source src="audio/New/Yo_Ve_-_Curiosity.ogg" type="audio/ogg">
			  <source src="audio/New/Yo_Ve_-_Curiosity.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="11first">Cyberstep</label>
			<audio name="11first" controls>
			  <source src="audio/New/Yo_Ve_-_Cyberstep.ogg" type="audio/ogg">
			  <source src="audio/New/Yo_Ve_-_Cyberstep.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="12first">Depth</label>
			<audio name="12first" controls>
			  <source src="audio/New/Yo_Ve_-_Depth.ogg" type="audio/ogg">
			  <source src="audio/New/Yo_Ve_-_Depth.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>

		    <label for="13first">Ergo Proxy</label>
			<audio name="13first" controls>
			  <source src="audio/New/Yo_Ve_-_Ergo_Proxy.ogg" type="audio/ogg">
			  <source src="audio/New/Yo_Ve_-_Ergo_Proxy.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="14first">Portal</label>
			<audio name="14first" controls>
			  <source src="audio/New/Yo_Ve_-_Portal.ogg" type="audio/ogg">
			  <source src="audio/New/Yo_Ve_-_Portal.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="15first">Randomized</label>
			<audio name="15first" controls>
			  <source src="audio/New/Yo_Ve_-_Randomized.ogg" type="audio/ogg">
			  <source src="audio/New/Yo_Ve_-_Randomized.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
			<label for="16first">Whatever</label>
			<audio name="16first" controls>
			  <source src="audio/New/Yo_Ve_-_Whatever.ogg" type="audio/ogg">
			  <source src="audio/New/Yo_Ve_-_Whatever.mp3" type="audio/mpeg">
			  Your browser does not support the audio tag.
			</audio>
		</div>
		-->
		<div class="clear_fix"></div>
		<div style="text-align: center;">
		<h2>PMT Out There Post Production Project</h2>
		<iframe width="560" height="315" src="https://www.youtube.com/embed/PldeZroJ3GU" frameborder="0" allowfullscreen style="
    margin: 0 auto; display: block;"></iframe>
		</div>
	</div>
</div>
<!-- ====================== End Discography  ====================================================== -->

<!--******************************FOOTER***************************************-->
<?php
    $file2 = 'includes/footer.php';
    if (file_exists($file2) && is_readable($file2))
            {   include($file2); }
?>