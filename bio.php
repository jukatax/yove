<?php
    $file = 'includes/header.php';
    if (file_exists($file) && is_readable($file))
            {   include($file); }
?>
<!--******************************END HEADER***************************************-->
	<nav>
		<a href='index.php' class='button'>Home</a>
		<a href='#' class='button selected'>Biography</a>
		<a href='discography.php' class='button'>Discography</a>
		<a href='contact.php' class='button'>Contact</a>
	</nav>

<!-- ====================== Bio  ====================================================== -->
<div class="wrapper_b">
	<div class="inner">
		<h1 class="h1d">Biography</h1>
		<p>Yordan Velichkov aka Ergo, is a twenty-four year-old Bulgarian music producer currently residing in London, United Kingdom. Coming from an artistic family with his father being a pianist in the army orchestra as well as his mother being an amateur painter, Yordan was naturally exposed to creative art forms from a young age. His music journey however didn’t start until he was first introduced to a digital audio workstation, by his uncle, when he was about fifteen years old. He immediately fell in love with the concept of creating music digitally as it gave him the opportunity to fully unleash his creative mind. Composing short beats on a daily basis quickly turned into a hobby of his and shortly after he became obsessed with electronic music production to the point of feeling the urge to study the art form professionally in order to improve his production skills and later on search for a job within the music industry.</p>
	<p>His style of music, as of yet, can be looked at as being experimental as he attempts to compose in many different genres of electronic music including Dubstep, Drum & Bass, House, Trance, Garage and Ambient. The character of his music can be described as being very melody and bass heavy, making it very suitable for games and movies.</p>
</div>
</div>
<!-- ====================== End Bio ====================================================== -->

<!--******************************FOOTER***************************************-->
<?php
    $file2 = 'includes/footer.php';
    if (file_exists($file2) && is_readable($file2))
            {   include($file2); }
?>