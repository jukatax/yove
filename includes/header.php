    <?php
    $title=basename($_SERVER['SCRIPT_NAME'],'.php');
	if($title=='index'){
		$title='Music Production';
	}
	else{
		$title=ucfirst($title);
		$title=str_replace('_',' ', $title);
	}
    
	$tel='020-&shy;89972450';
	$mob='075-&shy;19139995';
	$company_name='Ergo';
    $email='dj_frombg@yahoo.co.uk';	
    ini_set('date.timezone', 'Europe/London');
	$thisyear=date('y');
	$startyear=12;
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo "{$company_name}-{$title}";?></title>
		<meta charset="utf-8">
		<meta name='description'content='Music production service-YoVe-Bio' />
		<meta name="keywords" content="Music production" />
		<meta name="viewport" content="width=device-width, maximun-scale=1.0" />
		<meta name="author" content="Yuliyan Y - yuliyan-yordanov.co.uk" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<link rel="stylesheet" href="css/normalize.css" type="text/css" media="all">
		<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen">
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="js/scroll.js"></script>
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="js/html5.js"></script>
		   <script>
		       iefix(); //fix the lack of placeholders for forms in ie.
			  document.createElement('header');
			  document.createElement('nav');
			  document.createElement('section');
			  document.createElement('article');
			  document.createElement('aside');
			  document.createElement('footer');
			</script>
		<![endif]-->


	</head>
    <?php
    if($title=='Music Production'){
		echo "<body class='homeonly'>";
	}
	else{
		echo "<body>";
	}
    ?>
<!--******************************END HEADER***************************************-->