<?php
    $file = 'includes/header.php';
    if (file_exists($file) && is_readable($file))
            {   include($file); }
?>
<!--******************************END HEADER***************************************-->
	<nav class="home_nav">
		<a href='#' class='button selected'>Home</a>
		<a href='bio.php' class='button'>Biography</a>
		<a href='discography.php' class='button'>Discography</a>
		<a href='contact.php' class='button'>Contact</a>
	</nav>
	<div class="homelink"><a href="https://soundcloud.com/ergo-music" target="_blank"> </a></div>
<!--******************************FOOTER***************************************-->
<?php
    $file2 = 'includes/footer.php';
    if (file_exists($file2) && is_readable($file2))
            {   include($file2); }
?>